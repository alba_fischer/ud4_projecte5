package Tasca4;

public class Exercici5 {

	public static void main(String[] args) {
		// Declarem 4 variables i les anem sobreescrivint  
		int A = 1, B = 2, C = 3, D = 4;
		System.out.println("Inicialment A = " + A + " B = " + B + " C = " + C +" D = " + D);
		B = C = A = D = B;
		/*En aquest cas totes les variables tindran el mateix valor, per�
		*en el cas de realitzar les instruccions de forma individual: B = C; C = A; A = D; D = B;  el resultat
		*seria diferent*/
		
		System.out.println("Finalment A = " + A + " B = " + B + " C = " + C +" D = " + D);
	

	}

}
